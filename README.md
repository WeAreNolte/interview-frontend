# The Stompy Wine Grid

Welcome to the Stompy Wine Grid challenge!

## Step 1
Spin up the app's front-end:

```
cd frontend && npm i && npm start
```

The project should be running at http://localhost:3006/ with some static wines. You need to make them into a responsive grid like the below mockups:

### Mobile
![Mobile grid](assets/grid_mobile.png)

### Desktop
The grid should have 4 columns
![Desktop grid](assets/grid_desktop.png)


## Step 2

Now we're going to make the grid interactive. First spin up the mock API in a separate console tab:

```
cd api && npm i && npm start
```

Then run the following in a separate console tab to ensure it's working: `curl -X GET http://localhost:3000/box`. Your task now is to connect up the front-end to the API:

1. The grid should populate from the `GET http://localhost:3000/box` endpoint. This returns an array of Wine objects as follows:

```
{
    "wines": [
        {
            "slot_number": 1,
            "name": "Pegau Maclura",
            "primary_image": "https://placehold.co/400x800?text=Pegau+Maclura"
        },
        {
            "slot_number": 2,
            "name": "Paxton Shiraz",
            "primary_image": "https://placehold.co/400x800?text=Paxton+Shiraz"
        },
        ...
    ]
}
```

2. Hitting the swap button should call `PUT http://localhost:3000/box/swap` with `{"slot_number":1}` in the body. It should then replace the swapped slot with the new wine, returned in the following format:

```
{
    "slot_number": 1,
    "name": "Pegau Maclura",
    "primary_image": "https://placehold.co/400x800?text=Pegau+Maclura"
}
```

For simplicity, each slot can be swapped a maximum of once per session and cannot be swapped back.
