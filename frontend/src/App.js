import Card from './components/Card';

import './App.css';

function App() {

    const wines = [
        {
            name: 'Weingut Brand Weiss Pet Nat',
            imageUrl: 'https://cdn11.bigcommerce.com/s-yc5i2s6zzp/products/223/images/467/2TfllXwRXjNHd7I6uv8XcXlEXWyKY4-metad2hpdGUuanBlZw__-__11509.1630626717.386.513.jpg'
        },
        {
            name: 'Vega Valterra Bobal Natural',
            imageUrl: 'https://cdn11.bigcommerce.com/s-yc5i2s6zzp/products/222/images/466/h6SAA75boU09Rp8aitMhGOWxlVqpvS-metacmVkLmpwZWc_-__34965.1630626640.386.513.jpg'
        },
        {
            name: 'Untermoserhof Sankt Magdalena',
            imageUrl: 'https://cdn11.bigcommerce.com/s-yc5i2s6zzp/products/221/images/465/bstsSrJjsK0w76PAmM1cr7oyIQ4QAL-metacmVkLmpwZWc_-__92597.1630626561.386.513.jpg'
        },
        {
            name: 'Pizzolato Rose Prosecco',
            imageUrl: 'https://cdn11.bigcommerce.com/s-yc5i2s6zzp/products/216/images/460/W93nt4Ig6Y9CQSH6YKbkVXPvO8VrV3-metacm9zZS5qcGVn-__30303.1630626180.386.513.jpg'
        },
        {
            name: 'On Giant’s Shoulders Chardonnay',
            imageUrl: 'https://cdn11.bigcommerce.com/s-yc5i2s6zzp/products/210/images/454/DDcbbAbCR2eLh9G1SgNhundvkeANEM-metad2hpdGUuanBlZw__-__57405.1630625685.386.513.jpg'
        },
        {
            name: 'Metamorphika',
            imageUrl: 'https://cdn11.bigcommerce.com/s-yc5i2s6zzp/products/204/images/448/pmeLK9y3QWxZgIH6JfH79p1ZgybT7L-metab3JhbmdlLmpwZWc_-__41346.1630625128.386.513.jpg'
        }
    ];

    return (
        <div className="App">
            <section className="grid">
                {wines.map( (wine, index) =>
                    <Card key={index} title={wine.name} imageUrl={wine.imageUrl} />
                )}
            </section>
        </div>
    );
}

export default App;
