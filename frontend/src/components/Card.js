import React from 'react';

const Card = ({title, imageUrl}) => {
    return (
        <div className="card">
            <h5>{title}</h5>
            <img
                src={imageUrl}
                alt={`A bottle of ${title}`}
            />
            <button className="buttonPill">Swap</button>
        </div>
    );
};

export default Card;
