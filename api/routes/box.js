var express = require("express");
var router = express.Router();

router.get("/", function (req, res) {
  const wineList = [
    "Pegau Maclura",
    "Paxton Shiraz",
    "Niklas Lagrein",
    "Murmures Poulsard",
    "Deep Down Sauvignon Blanc",
    "Chateau Bel Air",
  ];

  return res.status(200).json({
    wines: wineList.map((wine, i) => {
      return {
        slot_number: i + 1,
        name: wine,
        primary_image: `https://placehold.co/400x800?text=${wine.replace(/\s/g, "+")}`,
      };
    }),
  });
});

router.put("/swap", function (req, res) {
  if (!req.body.slot_number || ![1, 2, 3, 4, 5, 6].includes(req.body.slot_number)) {
    return res.status(400).json({
      status: "error",
      message: "slot_number must be an integer from 1 to 6",
    });
  }

  const slotNumber = req.body.slot_number;

  const wineList = [
    "Du Loup Passetoutgrain",
    "Catena Malbec",
    "Cambridge Road Naturalist Blanc",
    "Beliviere Chinon",
    "A Tribute to Grace Rose",
    "Breton Trinch!",
  ];

  return res.status(200).json({
    slot_number: slotNumber,
    name: wineList[slotNumber - 1],
    primary_image: `https://placehold.co/400x800?text=${wineList[slotNumber - 1].replace(/\s/g, "+")}`,
  });
});

module.exports = router;
